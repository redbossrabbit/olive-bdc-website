const opt = document.getElementsByClassName('nav-links');
const ham = document.getElementById('ham');
const dropDown = document.getElementById('drop-down');
const black = document.getElementById('black');
ham.addEventListener('click', () => {
    DROP();
});
const DROP = () => {
    if (ham.classList.contains('is-open')) {
        dropDown.style.display = 'none';
        black.style.display = 'none';
        ham.classList.toggle('is-open');
    }else{
        dropDown.style.display = 'flex';
        black.style.display = 'flex';
        ham.classList.toggle('is-open');
    }
}